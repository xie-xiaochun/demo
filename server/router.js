let express = require('express')
let router = express.Router()
let info = require('./API/list')

router.get('/list/all', info.all)
router.get('/list/get', info.get)
router.get('/list/search', info.search)
router.post('/list/add', info.add)
router.post('/list/update', info.update)
router.get('/list/del', info.del)

module.exports = router